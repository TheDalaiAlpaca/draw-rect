// Draw a rectangle on your terminal screen

use std::io;
use std::process;
use terminal_size::{Width, Height, terminal_size};

// Read int from commandline
fn read_int() -> u16 {
    let my_int = loop {
        let mut my_int = String::new();
        io::stdin().read_line(&mut my_int)
            .expect("Failed to read line");
        let my_int = my_int.trim().parse();
        let _my_int: u16 = match my_int {
            Ok(i)   => {
                break i;
            },
            Err(..) => {
                println!("Not an integer. Please try again:");
                0
            },
        };
    };
    
    my_int
}

fn draw_rect(width: u16, height: u16, character: &str) {
    // Fill a horizontal bar
    
    let mut hori_bar: String = "".to_owned();

    for _i in 0..width {
        hori_bar.push_str(character);
    }

    println!("{}", hori_bar);


    // contruct midrow
    
    let mut midrow: String = "".to_owned();
    
    midrow.push_str(character);

    for _i in 0..(width - 2) {
        midrow.push_str(" ");
    }

    midrow.push_str(character);


    // Print midrows

    for _i in 0..(height - 2) {
        println!("{}", midrow);
    }

    // Print trailing horizontal bar
    
    println!("{}", hori_bar);

}

fn main() {

    println!("Please enter width:");
    let rect_width = read_int();

    println!("Please enter height:");
    let rect_height = read_int();

    let term_size = terminal_size();
    if let Some((Width(terminal_width), Height(terminal_height))) = term_size {
        if rect_width > terminal_width || rect_height > terminal_height {
            println!("Too large!");
            process::exit(1);
        }
    } else {
        println!("Unable to determine terminal size");
        process::exit(2);
    }

    

    let character: &str = "█";

    draw_rect(rect_width, rect_height, character);

    process::exit(0);
}
