Draws a rectangle on your terminal screen with sizes based on user input.

Build with:

`cargo build`

run with:

`./draw_rect/target/debug/draw_rect`